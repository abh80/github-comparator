import { parse } from "yaml";
import { join } from "path";
import { existsSync, readFileSync } from "fs";
import Logger from "./Logger.js";
import { IConfig } from "../typings/types";

export default function parseYML(
  configPath: string = join(process.cwd(), "config.yml"),
): IConfig {
  if (!existsSync(configPath)) {
    Logger.error(`fatal : \`config.yml\` not found at path \`${configPath}\``);
    throw new Error("`config.yml` is not present in root directory");
  }

  return parse(readFileSync(configPath, { encoding: "ascii" })) as IConfig;
}
