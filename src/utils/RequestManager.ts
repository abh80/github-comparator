import { GraphQLClient, gql } from "graphql-request";
import {
  RequestConfig,
  RequestDocument,
} from "graphql-request/build/esm/types";
import { IPendingRequestMap } from "typings/types";

import Logger from "./Logger.js";

import EventEmitter from "events";
import { AxiosError } from "axios";

export default class RequestManager extends EventEmitter {
  private readonly client: GraphQLClient;
  private queue: IPendingRequestMap[];
  private timeout: NodeJS.Timeout | null;

  constructor(apiUrl: string, config: RequestConfig) {
    super();
    this.client = new GraphQLClient(apiUrl, {
      headers: config.headers,
    });
    this.queue = [];
    this.timeout = null;

    this.emit("ready!");
  }

  public queueReq(req: RequestDocument): void {
    this.queue.push({
      requestBody: req,
      timeoutClear: 0,
    });
    this.handleNext();
  }

  private handleNext() {
    if (this.timeout) return;
    if (this.queue.length <= 0) return;
    const target = this.queue[0];
    this.timeout = setTimeout(() => {
      this.send();
      this.timeout = null;
    }, target.timeoutClear);
  }

  private log(message: string, error: unknown): void {
    Logger.error(`[RequestManager] ${message}`);
    Logger.debug(
      `[RequestManager] Error debug output : ${
        typeof error == "object" ? JSON.stringify(error) : error
      }`,
    );
  }

  public async send(): Promise<void> {
    if (this.queue.length <= 0) return;
    const target = this.queue[0];
    try {
      const data = await this.client.request(this.queue[0].requestBody);
      this.queue.shift();
      this.emit("data", data);
      Logger.debug("Emitted data");
      this.handleNext();
    } catch (e) {
      const response = (e as AxiosError).response;
      if (!response)
        return void this.log(
          "Response is null, probably issue related to network connection. Check the network avalibility again!",
          e,
        );

      if (response.status != 429)
        return void this.log(
          `Response status is ${response.status}. Which was not meant to be handled by RequestManager! Check if your credentials are correct.`,
          e,
        );

      let header: string | null = null;
      if (response.headers.get && typeof response.headers.get == "function") {
        header = response.headers.get("x-ratelimit-reset") as unknown as string;
      }

      if (!header)
        return void this.log(
          `\`x-ratelimit-reset\` header is not present, unable to get ratelimit duration`,
          e,
        );

      try {
        const timestamp = parseInt(header);
        const diff = timestamp - Date.now();
        this.queue[0] = { ...target, timeoutClear: diff < 0 ? 0 : diff };

        if (diff > 0)
          Logger.debug(
            `[RequestManager] Ratelimit hit, awaiting for ${diff}, ms...`,
          );
        this.handleNext();
      } catch {
        return void this.log(
          "`x-ratelimit-reset` header is not a number, unable to get ratelimit duration",
          e,
        );
      }
    }
  }
}
