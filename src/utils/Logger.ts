import { createLogger, format, transports } from "winston";
import { join } from "path";
import * as Transport from "winston-transport";
import { LEVEL, MESSAGE } from "triple-beam";

const transportArray: Transport[] = [
  new transports.File({
    filename: "error.log",
    level: "error",
    dirname: join(process.cwd(), "..", "..", "logs"),
  }),
  new transports.File({
    filename: "combined.log",
    dirname: join(process.cwd(), "logs"),
  }),
];

if (process.env.NODE_ENV !== "production") {
  transportArray.push(
    new transports.Console({
      format: format.simple(),
    }),
  );
}

export default createLogger({
  level: process.env.NODE_ENV === "production" ? "info" : "debug",
  format: format.combine(
    format.colorize(),
    format.printf((i) => `${[i[LEVEL]]} : ${i[MESSAGE]}`),
  ),
  transports: transportArray,
});
