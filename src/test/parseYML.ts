import { assert, expect } from "chai";
import parseYML from "../utils/parseYML.js";
import { join } from "path";

describe("Test YML Parser", () => {
  const data = parseYML(join(process.cwd(), "config.test.yml"));

  it("Should be an Object", () => {
    assert.isObject(data);
  });

  it("Should be valid YML", () => {
    expect(data).deep.equal({
      task: {
        generate_training_targets: {
          api_url: "https://api.github.com",
          endpoint: "/search/user",
          params: [
            { followers: ">0" },
            { per_page: 1000 },
            { sort: "followers" },
            { order: "desc" },
          ],
        },
      },
    });
  });
});
