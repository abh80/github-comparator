import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { IConfig } from "../typings/types";
import Logger from "../utils/Logger";

export default async function getTrainingUsersList(
  options: IConfig,
): Promise<object[]> {
  Logger.debug("Getting Training List from `config.yml`");

  return [];
}

function doRequest(
  url: string,
  options: AxiosRequestConfig,
): Promise<AxiosResponse> {
  return axios.get(url, { ...options });
}
