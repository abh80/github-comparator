import { RequestDocument } from "graphql-request";

export interface IConfig {
  readonly task: {
    readonly generate_training_targets: {
      readonly api_url: string;
      readonly endpoint: string;
      readonly params: object;
      readonly training_size: number;
    };
  };
}

export interface IPendingRequestMap {
  timeoutClear: number;
  requestBody: RequestDocument;
}
