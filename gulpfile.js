import gulp from "gulp";
import ts from "gulp-typescript";

gulp.task("build:test", () => {
  const project = ts.createProject("tsconfig.json");

  return gulp
    .src("src/test/*.ts")
    .pipe(project())
    .js.pipe(gulp.dest("dist/test"));
});

gulp.task("build:task", () => {
  const project = ts.createProject("tsconfig.json");

  return gulp
    .src("src/task/*.ts")
    .pipe(project())
    .js.pipe(gulp.dest("dist/task"));
});

gulp.task("build:util", () => {
  const project = ts.createProject("tsconfig.json");

  return gulp
    .src("src/utils/*.ts")
    .pipe(project())
    .js.pipe(gulp.dest("dist/utils"));
});

gulp.task("watch:util", () => {
  gulp.watch("src/utils/*.ts", gulp.series("build:util"));
});

// Watch task for "task" files
gulp.task("watch:task", () => {
  gulp.watch("src/task/*.ts", gulp.series("build:task"));
});

// Watch task for "test" files (assuming you also want to watch test files)
gulp.task("watch:test", () => {
  gulp.watch("src/test/*.ts", gulp.series("build:test"));
});

gulp.task("build", gulp.parallel("build:util", "build:task"));

gulp.task(
  "watch",
  gulp.series("build", gulp.parallel("watch:util", "watch:task"))
);
