# Github-Comparator
This project is just came from the thinking that "what is the perfect way to classify to Github accounts and find which is better".

# Installation

- Run the git clone command to clone the repository and cd into the folder
    ```bash
    $ git clone https://gitlab.com/abh80/github-comparator.git
    cd github-comparator
    ```
- Optional (1/3): Using Vagrant [Recommended]
  
  The [Vagrant](https://www.vagrantup.com/) file in this repository comes with all the predefined dependencies need to debug and run this project!

  This method requires you to have the [Oracle VirtualBox](https://www.virtualbox.org/) installed on your system.

  Simply run
  ```bash
  $ vagrant up --provision 
  ```

  🎉 Everything is setup!

  Skip the remaining installation steps

  To get into the VM run `vagrant ssh`
- Optional (2/3): Dev Container
  
  This repository also comes with [Dev Container](https://containers.dev/) file which can used to debug and run this application.

  Learn how to open this project in a container from the [VSCode Guide](https://code.visualstudio.com/docs/devcontainers/containers).
- Optional (3/3): Running on Host OS
  
  ## Prequisites
  - node.js
  - a node package manager such as yarn (recommended) or npm
  - [Tensorflow required dependencies](https://www.tensorflow.org/install)
  
  ## Install dependencies
  - Use `yarn` or `npm install`
  - Run `yarn build` or `npm run build`